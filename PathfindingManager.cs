﻿using System;
using System.Collections.Generic;
using System.Threading;

namespace Astar
{
    public class PathfindingManager
    {
        const int _poolSize = 8;
        Pool<Pathfinding> _pool = new Pool<Pathfinding>(_poolSize, p => new Pathfinding(new Grid(new Tuple<float, float>(Program.x, Program.y), 1f, 768)));
        Queue<PathingRequest> _queue = new Queue<PathingRequest>();

        public void RequestPath(Tuple<float, float> from, Tuple<float, float> to, int requestId, Action<List<Tuple<float, float>>, int> callback)
        {
            _queue.Enqueue(new PathingRequest { From = from, To = to, Id = requestId, Callback = callback });
        }       

        public void Update()
        {
            if (_pool.Count <= _poolSize && _queue.Count > 0)
            {
                var r = _queue.Dequeue();
                if (r != null)
                {
                    ThreadStart threadStart = delegate
                    {
                        using (var pathfinding = _pool.Acquire())
                        {                                             
                            pathfinding.FindPathPriority(r.From, r.To, r.Id, r.Callback);
                            _pool.Release(pathfinding);
                        }                                             
                    };
                    new Thread(threadStart).Start();
                }                
            }            
        }
    }

    public class PathingRequest
    {
        public Tuple<float, float> From { get; set; }

        public Tuple<float, float> To { get; set; }

        public Action<List<Tuple<float, float>>, int> Callback { get; set; }

        public int Id { get; set; }
    }
}
