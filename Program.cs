﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Astar
{
    class Program
    {
        public static readonly int x = 1000;
        public static readonly int y = 1000;
        static void Main(string[] args)
        {
            var pathfindingManager = new PathfindingManager();
            var grid = new Grid(new Tuple<float, float>(x, y), 10f, 768);
            var r = new Random();
            for (int i = 0; i < 1000; i++)
            {                
                var xx = r.Next(0, x);
                var yy = r.Next(0, y);

                Console.WriteLine($"Lägger till request från {Thread.CurrentThread.ManagedThreadId}");
                pathfindingManager.RequestPath(new Tuple<float, float>(0, 0), new Tuple<float, float>(xx, yy), i, PrintResult);
            }

            while(true)
            {
                pathfindingManager.Update();
            }
        }

        private static void PrintResult(List<Tuple<float, float>> path, int id)
        {
            if(path == null)
                Console.WriteLine($"Fanns ingen path för denna");
            else
                Console.WriteLine($"{id} :: Fick en {path.Count} lång path");
        }
    }
}
