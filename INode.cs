﻿namespace Astar
{
    public interface INode<T>
    {
        int Priority { get; }
        int QueueIndex { get; set; }
    }
}
