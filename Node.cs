﻿using System;

namespace Astar
{
    public class Node: INode<Node>
    {
        public bool walkable;
        public Tuple<float, float> worldPosition;
        public int gridX;
        public int gridY;

        public int gCost;
        public int hCost;
        public Node parent;

        public Node(bool _walkable, Tuple<float, float> _worldPos, int _gridX, int _gridY)
        {
            walkable = _walkable;
            worldPosition = _worldPos;
            gridX = _gridX;
            gridY = _gridY;
        }

        public int fCost
        {
            get
            {
                return gCost + hCost;
            }
        }

        public int QueueIndex { get; set; }

        public int Priority
        {
            get
            {
                return fCost;
            }
        }

        public override string ToString()
        {
            return $"({worldPosition.Item1}, {worldPosition.Item2})";
        }

        public int CompareTo(Node other)
        {
            int compare = fCost.CompareTo(other.fCost);

            if (compare == 0)
            {
                compare = hCost.CompareTo(other.hCost);
            }

            return -compare;
        }
    }
}
