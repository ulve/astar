﻿using System;
using System.Collections.Generic;

namespace Astar
{
    public class Grid : IDisposable
    {
        Tuple<float, float> gridWorldSize;
        float nodeRadius;
        Node[,] grid;
        public int MaxSize { get; private set;}
        float nodeDiameter;
        int gridSizeX, gridSizeY;
        
        public Grid(Tuple<float, float> gws, float nodeRadius, int seed = 0)
        {
            this.nodeRadius = nodeRadius;
            gridWorldSize = gws;
            nodeDiameter = nodeRadius * 2;
            gridSizeX = Convert.ToInt32(gridWorldSize.Item1 / nodeDiameter);
            gridSizeY = Convert.ToInt32(gridWorldSize.Item2 / nodeDiameter);
            MaxSize = gridSizeX * gridSizeY;

            CreateGrid(seed);
            var w = 0;
            foreach (var n in grid)
                if (n.walkable) w = w + 1;


            Console.WriteLine($"Walkable {(Convert.ToDouble(w) / grid.Length) * 100} %");
        }

        void CreateGrid(int seed)
        {
            var r = new Random(seed);
            grid = new Node[gridSizeX, gridSizeY];

            for (int x = 0; x < gridSizeX; x++)
            {
                for (int y = 0; y < gridSizeY; y++)
                {
                    Tuple<float, float> worldPoint = new Tuple<float, float>((x * nodeDiameter + nodeRadius), (y * nodeDiameter + nodeRadius));

                    var walkable = r.Next(0, 100) > 5;

                    grid[x, y] = new Node(walkable, worldPoint, x, y);
                }
            }
        }

        public List<Node> GetNeighbours(Node node)
        {
            List<Node> neighbours = new List<Node>();

            for (int x = -1; x <= 1; x++)
            {
                for (int y = -1; y <= 1; y++)
                {
                    if (x == 0 && y == 0)
                        continue;

                    int checkX = node.gridX + x;
                    int checkY = node.gridY + y;

                    if (checkX >= 0 && checkX < gridSizeX && checkY >= 0 && checkY < gridSizeY)
                    {
                        neighbours.Add(grid[checkX, checkY]);
                    }
                }
            }

            return neighbours;
        }        

        public Node NodeFromWorldPoint(Tuple<float, float> worldPosition)
        {
            int x = Convert.ToInt32((gridSizeX - 1) * ((worldPosition.Item1) / gridWorldSize.Item1));
            int y = Convert.ToInt32((gridSizeY - 1) * ((worldPosition.Item2) / gridWorldSize.Item2));
            return grid[x, y];
        }

        public void Dispose()
        {
          
        }        
    }
}